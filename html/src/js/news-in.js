// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {}
});

let newsInAni = new TimelineLite();
newsInAni
  .from(".main__img", 0.5, {
    delay: 1,
    x: -30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".font__newsin", 0.5, {
    x: -30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".newsin__cco>.container", 0.5, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  });
