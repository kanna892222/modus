// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {}
});
//先把他預設加上on=>class
let tabItem = document.querySelectorAll(".tabs a");
$(tabItem[0]).addClass("on");

let tabsSwiper = new Swiper(".swiper-tabs1", {
  speed: 1000,
  onSlideChangeStart: function() {
    $(".tabs .on").removeClass("on");
    $(".tabs a")
      .eq(tabsSwiper.activeIndex)
      .addClass("on");
  }
});

$(".tabs a").on("touchstart mousedown", function() {
  let _ = $(this);
  _.addClass("on")
    .siblings()
    .removeClass("on");
  tabsSwiper.slideTo(_.index());
  return false;
});

$(".list__dsk>a").removeClass("active");
$("#news").addClass("active");

let mainImgNum;
let mainImgFn = function() {
  if (window.matchMedia(bkpMbl).matches) {
    return (mainImgNum = "100%");
  } else {
    return (mainImgNum = "69.5312%");
  }
};
mainImgFn();
//進場動畫

if (window.matchMedia(bkpMbl).matches) {
  window.addEventListener("load", function() {
    setTimeout(() => {
      $(".blue__box").removeClass("aniMbl");
    }, 3000);
  });
}

let newsAni = new TimelineLite();
newsAni
  .to(".news__msk", 2, {
    delay: 1,
    scaleX: 0,
    transformOrigin: "100% 50%"
  })
  .to(".news__mainimg", 1, {
    opacity: 1,
    width: mainImgNum,
    clearProps: "opacity, transform"
  })
  .to(".blue__blue", 1, {
    delay: -0.5,
    scaleX: 1,
    opacity: 1,
    transformOrigin: "100% 50%",
    ease: Power4.easeOut
  })
  .from(".news__title", 0.5, {
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .staggerFrom(
    ".en__txtAn1",
    0.8,
    {
      left: "+=100",
      transform: "rotateY(180deg)",
      opacity: 0,
      clearProps: "opacity, transform"
    },
    0.15
  )
  // .staggerFrom(
  //   ".ch__txtAn1",
  //   0.8,
  //   {
  //     left: "+=100",
  //     transform: "rotateY(180deg)",
  //     opacity: 0,
  //     clearProps: "opacity, transform"
  //   },
  //   0.15
  // )
  .from(".cn__title", 1, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".font__news", 1, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  });

//進場動畫
// let aboutAni = new TimelineLite();
// aboutAni
//   .to(".about__msk", 2, {
//     delay: 1,
//     scaleX: 0,
//     transformOrigin: "100% 50%"
//   })
//   .to(".about__mainimg", 1, {
//     opacity: 1,
//     width: mainImgNum,
//     clearProps: "opacity, transform"
//   })
//   .to(".blue__blue", 2, {
//     delay: -1.8,
//     scaleX: 1,
//     opacity: 1,
//     transformOrigin: "100% 50%",
//     ease: Power4.easeOut
//   })
//   .from(".img__title", 0.5, {
//     y: 30,
//     opacity: 0,
//     clearProps: "opacity, transform"
//   })
//   .from(".modus__fontimg", 0.5, {
//     x: 30,
//     opacity: 0,
//     clearProps: "opacity, transform"
//   })
//   .staggerFrom(
//     ".en__txtAn1",
//     0.8,
//     {
//       left: "+=100",
//       transform: "rotateY(180deg)",
//       opacity: 0,
//       clearProps: "opacity, transform"
//     },
//     0.15
//   )
//   .staggerFrom(
//     ".ch__txtAn1",
//     0.8,
//     {
//       left: "+=100",
//       transform: "rotateY(180deg)",
//       opacity: 0,
//       clearProps: "opacity, transform"
//     },
//     0.15
//   )
//   .from(".msg__txt", 0.5, {
//     y: 30,
//     opacity: 0,
//     clearProps: "opacity, transform"
//   });

let controller = new ScrollMagic.Controller({
  //addIndicators: true
});
//中間區塊
let newsScrollAni = new TimelineLite();
newsScrollAni.from(".tab__cco", 1, {
  y: 30,
  opacity: 0,
  clearProps: "opacity, transform"
});
let newsScroll = new ScrollMagic.Scene({
  triggerElement: ".news__tabsslide", //觸發點
  triggerHook: 0.8,
  reverse: !1
})
  .setTween(newsScrollAni)
  .addTo(controller);

//最下面區塊
let newsScrollAni2 = new TimelineLite();
newsScrollAni2.from(".news__cco", 1, {
  y: 30,
  opacity: 0,
  clearProps: "opacity, transform"
});

let newsScroll2 = new ScrollMagic.Scene({
  triggerElement: ".news__cco", //觸發點
  triggerHook: 0.85,
  reverse: !1
})
  .setTween(newsScrollAni2)
  .addTo(controller);
