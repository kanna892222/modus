let em = new Vue({
  el: "#commonFooter",
  data: {
    commonData
  }
});

//==============================================鎖定卷軸
//==============================================鎖定卷軸
let scrollNum;
// //鎖定卷軸

//==============================================選單按鈕用
//==============================================選單按鈕用
$(document).ready(function() {
  $(".menu__btnopen").on("click", function() {
    $(".layout__list").toggleClass("active");
    scrollNum = $(window).scrollTop();
    $("body").css({ top: scrollNum * -1 });
    $("html").addClass("active");
  });
  $(".menu__btnclose").on("click", function() {
    $(".layout__list").toggleClass("active");
    $("html").removeClass("active");
    $("body").css({ top: "" });
    $("html,body").scrollTop(scrollNum);
  });
});

//==============================================回到頂端按鈕用
//==============================================回到頂端按鈕用
//perpos需要引入才可以使用
window.addEventListener("load", () => {
  let topUP = document.querySelector("#upTop");
  topUP.addEventListener(
    "click",
    () => {
      TweenLite.to(window, 2, {
        scrollTo: { y: 0, x: 0, autoKill: false },
        ease: Power4.easeOut
      });
    },
    false
  );
});

document.addEventListener(
  "DOMContentLoaded",
  e => {
    $("html").addClass("active");
    setTimeout(() => {
      $(".full__loading").css({ opacity: "0" });
      $("html").removeClass("active");
    }, 1500);
  },
  false
);
