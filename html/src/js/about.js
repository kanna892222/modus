// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {}
});

let swiper = new Swiper(".his_swiper-container", {
  navigation: {
    nextEl: ".his-next",
    prevEl: ".his-prev"
  },
  //循環
  loop: true,
  slidesPerView: "4",
  breakpoints: {
    1199: {
      //寬度<=1199
      slidesPerView: 1
    }
  },
  speed: 500
  //自動播放
  // autoplay: {
  //   delay: 5000
  // }
});

//brand區域顯示輪播

let galleryTop = new Swiper(".gallery-top", {
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev"
  },
  loop: true,
  loopedSlides: 4,
  effect: "fade",
  fadeEffect: { crossFade: true },
  speed: 1000
});
let galleryThumbs = new Swiper(".gallery-thumbs", {
  touchRatio: 0.2,
  loop: true,
  loopedSlides: 4,
  slidesPerView: "2",
  spaceBetween: 40,
  speed: 1000,
  breakpoints: {
    1199: {
      //寬度<=1199
      slidesPerView: 1,
      spaceBetween: 0
    }
  }
});
galleryTop.controller.control = galleryThumbs;
galleryThumbs.controller.control = galleryTop;

$(".list__dsk>a").removeClass("active");
$("#about").addClass("active");

//動畫rwd設定
let mainImgNum;
let mainImgFn = function() {
  if (window.matchMedia(bkpMbl).matches) {
    return (mainImgNum = "92.1875%");
  } else {
    return (mainImgNum = "69.2708%");
  }
};
mainImgFn();

window.addEventListener("load", function() {});

if (window.matchMedia(bkpMbl).matches) {
  window.addEventListener("load", function() {
    setTimeout(() => {
      $(".blue__box").removeClass("aniMbl");
    }, 3000);
  });
}
let aboutBgc;
if (window.matchMedia(bkpDsk).matches) {
  aboutBgc = "-100px 0px";
} else {
  aboutBgc = "0px 0px";
}

// let aboutAni2 = new TimelineLite();
// aboutAni2.to(".mask__pic", 1.2, {
//   width: 0,
//   opacity: 0,
//   clearProps: "opacity, transform"
// });
//進場動畫
// setTimeout(() => {
//
// }, 1000);

setTimeout(() => {
  let testaa = new TimelineLite();
  testaa.to(".about__mainimg", 2, {
    // left: 0
    delay: -0.5,
    // backgroundSize: "100%"
    backgroundPosition: "-100px 0px",
    ease: Power3.easeOut
    // backgroundPosition: aboutBgc
  });
}, 1800);

if (window.matchMedia(bkpMbl).matches) {
  setTimeout(() => {
    let testaa = new TimelineLite();
    testaa.to(".about__mainimg", 2, {
      // left: 0
      delay: -0.5,
      // backgroundSize: "100%"
      backgroundPosition: "0px 0px",
      ease: Power3.easeOut
      // backgroundPosition: aboutBgc
    });
  }, 1800);
}

setTimeout(() => {
  let aboutAni = new TimelineLite();
  aboutAni
    .to(".about__mainimg", 1, {
      left: 0
      // right: "60.7292%",
      // backgroundPosition: "0px 0px",
      // transform: "translate3d(0,0,0)"
    })

    .to(".blue__box", 1.55, {
      delay: -1,
      width: mainImgNum,
      clearProps: "opacity, transform"
    })

    .to(".mask", 1.2, {
      delay: -1.2,
      width: 0
    })

    .from(".img__title", 0.5, {
      delay: 1,
      y: 30,
      opacity: 0,
      clearProps: "opacity, transform"
    })
    .from(".modus__fontimg", 0.5, {
      delay: -1,
      x: 30,
      opacity: 0,
      ease: Power4.easeOut,
      clearProps: "opacity, transform"
    })
    .staggerFrom(
      ".en__txtAn1",
      0.8,
      {
        left: "+=100",
        transform: "rotateY(180deg)",
        opacity: 0,
        clearProps: "opacity, transform"
      },
      0.15
    )
    .from(".ab__an2", 0.5, {
      y: 30,
      opacity: 0,
      clearProps: "opacity, transform"
    })

    .from(".msg__txt", 0.5, {
      y: 30,
      opacity: 0,
      clearProps: "opacity, transform"
    });
}, 1500);

//捲軸用
let controller = new ScrollMagic.Controller({
  //addIndicators: true
});
//中間區塊
let aboutScrollAni1 = new TimelineLite();
aboutScrollAni1.from(".about__his", 1, {
  y: 30,
  opacity: 0,
  clearProps: "opacity, transform"
});
let aboutScroll1 = new ScrollMagic.Scene({
  triggerElement: ".about__his", //觸發點
  triggerHook: 0.7,
  reverse: !1
})
  .setTween(aboutScrollAni1)
  .addTo(controller);

//藍色輪胎區域
let aboutScrollAni2 = new TimelineLite();
aboutScrollAni2.from(".about__brandslide", 1, {
  delay: 0.2,
  y: 30,
  opacity: 0,
  clearProps: "opacity, transform"
});
let aboutScroll2 = new ScrollMagic.Scene({
  triggerElement: " .about__brandslide", //觸發點
  triggerHook: 0.65,
  reverse: !1
})
  .setTween(aboutScrollAni2)
  .addTo(controller);
//樹葉區塊
let aboutScrollAni3 = new TimelineLite();
aboutScrollAni3.from(".about__brandinfo", 1, {
  delay: 0.2,
  y: 30,
  opacity: 0,
  clearProps: "opacity, transform"
});
let aboutScroll3 = new ScrollMagic.Scene({
  triggerElement: " .about__brandinfo", //觸發點
  triggerHook: 0.75,
  reverse: !1
})
  .setTween(aboutScrollAni3)
  .addTo(controller);

tl = new TimelineLite();
// tl.staggerFrom(
//   mySplitText[property],
//   1,
//   { opacity: 0, rotationY: 180, ease: Elastic.easeOut },
//   0.4
// );
