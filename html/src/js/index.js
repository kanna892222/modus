// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData, lightBoxIndex: 0, lightBoxStatus: false },
  mounted() {}
});

let index_swiper = new Swiper(".index_swiper-container", {
  init: false,
  navigation: {
    nextEl: ".index-next",
    prevEl: ".index-prev"
  },
  //循環
  loop: true,
  effect: "fade",
  fadeEffect: {
    crossFade: true
  },
  lazy: {
    loadPrevNext: true
  },
  speed: 1000,
  autoplay: {
    delay: 8000,
    disableOnInteraction: false
  },
  on: {
    slideChangeTransitionStart: function() {
      let title1 = pageData.se1[this.realIndex].title1;
      let title2 = pageData.se1[this.realIndex].title2;
      let splitTop = title1.split("");
      let splitBottom = title2.split("");
      $(".info__imgmask").show();
      $(".top").empty();
      $(".bottom").empty();
      splitTop.forEach(ele => {
        $(".top").append($("<span class='type__x'>").text(ele));
      });
      splitBottom.forEach(ele => {
        $(".bottom").append($("<span class='type__x'>").text(ele));
      });
      $(".top>.type__x").each(function(index, element) {
        setTimeout(() => {
          $(this).addClass("flip");
        }, 50 * index);
      });
      $(".bottom>.type__x").each(function(index, element) {
        setTimeout(() => {
          $(this).addClass("flip");
        }, 80 * index);
      });
    },
    slideChangeTransitionEnd: function() {
      let numLen = pageData.se1.length;
      let num = this.realIndex + 1;
      if (num === numLen) {
        let friMsg = pageData.se1[0].info;
        $(".next__msg").html(friMsg);
      } else {
        let nextMsg = pageData.se1[num].info;
        $(".next__msg").html(nextMsg);
      }
      $(".type__x").removeClass("flip");
      $(".type__x").each(function(index, element) {
        setTimeout(() => {
          $(this).addClass("flip");
        }, 100 * index);
      });
    }
  }
});

// let indexAniSlideEnd = new TimelineLite();
//       indexAniSlideEnd.to(".info__img", 2, {
//         delay: 1,
//         width: 0,
//         transformOrigin: "0% 50%"
// });
//card黑色區域輪播=>手機版才會啟動
if (window.matchMedia(bkpMbl).matches) {
  window.addEventListener("load", function() {
    let card_swiper = new Swiper(".card_swiper-container", {
      pagination: {
        //綁定點點可以按
        el: ".card_swiper-pagination",
        clickable: true
      }
    });
  });
}

//case區域輪播=>手機版才會啟動
if (window.matchMedia(bkpMbl).matches) {
  window.addEventListener("load", function() {});
}
let case_swiper = new Swiper(".case_swiper-container", {
  pagination: {
    //綁定點點可以按
    el: ".case_swiper-pagination",
    clickable: true
  },
  spaceBetween: 70,
  slidesPerView: "3",
  breakpoints: {
    1199: {
      //寬度<=1199
      slidesPerView: 1,
      spaceBetween: 0
    }
  }
});

///桌機還沒改成輪播～

let award_swiper = new Swiper(".award_swiper-container", {
  pagination: {
    //綁定點點可以按
    el: ".award_swiper-pagination",
    clickable: true
  },
  watchOverflow: true
  //循環
  //自動播放
  // autoplay: {
  //   delay: 5000
  // }
});
//燈箱tab區域
let totalBtnLink = [...document.querySelectorAll(".card__link")];
let tabAdd = [...document.querySelectorAll(".tabs a")];
// console.log(tabAdd);
// // console.log(totalBtnLink);
totalBtnLink.forEach((ele, index) => {
  // console.log(ele);
  ele.addEventListener("click", function() {
    $(".tabs .on").removeClass("on");
    $(tabAdd[index]).addClass("on");
    $(".light__box").addClass("on");
    let tabsSwiper = new Swiper(".swiper-tabs1", {
      onSlideChangeStart: function() {
        $(".tabs .on").removeClass("on");
        $(".tabs a")
          .eq(tabsSwiper.activeIndex)
          .addClass("on");
      }
    });

    tabsSwiper.slideTo([index], 1000, false);

    $(".tabs a").on("touchstart mousedown", function() {
      let _ = $(this);
      _.addClass("on")
        .siblings()
        .removeClass("on");
      tabsSwiper.slideTo(_.index());
      return false;
    });

    let lightbox_swiper = new Swiper(".light_swiper-container", {
      navigation: {
        nextEl: ".light-next",
        prevEl: ".light-prev"
      }
    });
  });
});

//燈箱關閉
$(".close").on("click", function() {
  $(".light__box").removeClass("on");
});

if (window.matchMedia(bkpDsk).matches) {
  window.addEventListener("load", function() {
    let indexAni = new TimelineLite();
    indexAni.staggerFrom(
      ".card_swiper-slide",
      0.8,
      {
        y: "+=50",
        opacity: 0,
        clearProps: "opacity, transform"
      },
      0.5
    );
  });
}

//=========================================捲軸區域
//=========================================捲軸區域
// 捲軸事件
let controller = new ScrollMagic.Controller({
  //addIndicators: true
});
//case區域
let indexCardAni = new TimelineLite();
indexCardAni.staggerFrom(
  ".card_slider__info",
  0.8,
  {
    y: "+=100",
    opacity: 0,
    clearProps: "opacity, transform"
  },
  0.15
);

let indexcardScroll = new ScrollMagic.Scene({
  triggerElement: ".card_swiper-container", //觸發點
  triggerHook: 0.7,
  reverse: !1
})
  .setTween(indexCardAni)
  .addTo(controller);

let indexCaseAni = new TimelineLite();
indexCaseAni
  .from(".case__title", 1, {
    delay: 0.5,
    x: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".case__info", 1, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".case__container", 1, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  });

let indexcaseScroll = new ScrollMagic.Scene({
  triggerElement: ".index__caesarea", //觸發點
  triggerHook: 0.75,

  reverse: !1
})
  .setTween(indexCaseAni)
  .addTo(controller);

//award區域

let indexAwardAni = new TimelineLite();
indexAwardAni
  .from(".award__slide", 0.7, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".index__linknews", 0.7, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  });

let indexAwardScroll = new ScrollMagic.Scene({
  triggerElement: ".index__awardarea", //觸發點
  triggerHook: 0.75,
  reverse: !1
})
  .setTween(indexAwardAni)
  .addTo(controller);

//讀取動畫
let title1 = pageData.se1[0].title1;
let title2 = pageData.se1[0].title2;
let splitTop = title1.split("");
let splitBottom = title2.split("");
$(".top").empty();
$(".bottom").empty();
$(".info__imgmask").hide();

let aniWidth;
if (window.matchMedia(bkpDsk).matches) {
  aniWidth = "65%";
} else {
  aniWidth = "100%";
  $(".info__txtmask").hide();
}

window.addEventListener("load", function() {
  let beginAni = new TimelineLite();
  beginAni
    .to(".index__slide__infoImg", 1.9, {
      delay: 1.5,
      width: aniWidth,
      ease: Power4.easeOut,
      duration: 2.5
    })
    .to(".info__txtmask", 1, {
      delay: -1.4,
      width: "35%",
      ease: Power3.easeOut
    })
    .to(".info__txtmask", 0, {
      delay: -0.4,
      opacity: 0,
      ease: Power3.easeOut
    })
    .from(".top__sliderbtn", 1, {
      delay: 3,
      y: 30,
      opacity: 0,
      clearProps: "opacity, transform"
    });
  setTimeout(() => {
    index_swiper.init();
  }, 1500);
  setTimeout(() => {
    $(".top").empty();
    $(".bottom").empty();

    splitTop.forEach(ele => {
      $(".top").append($("<span class='type__x'>").text(ele));
    });
    splitBottom.forEach(ele => {
      $(".bottom").append($("<span class='type__x'>").text(ele));
    });
    $(".top>.type__x").each(function(index, element) {
      setTimeout(() => {
        $(this).addClass("flip");
      }, 50 * index);
    });
    $(".bottom>.type__x").each(function(index, element) {
      setTimeout(() => {
        $(this).addClass("flip");
      }, 80 * index);
    });
    let test1 = new TimelineLite();
    test1.from(".index__slide__txt", 1.5, {
      delay: 0.8,
      x: 300,
      opacity: 0,
      clearProps: "opacity, transform",
      ease: Power3.easeOut
    });
    $(".info__txtmask").hide();
  }, 2000);
});
setTimeout(() => {
  if (window.matchMedia(bkpDsk).matches) {
    $(".index__slide__infoImg").css("width", "65%");
  } else {
    $(".index__slide__infoImg").css("width", "100%");
  }
}, 4000);

//services 移動到第二個畫面用區
let topSe2 = document.querySelector("#gotoSe2");
topSe2.addEventListener(
  "click",
  () => {
    // console.log("a");
    const target = $(".index__caesarea");
    //選到第二個篇幅
    TweenLite.to(window, 1, {
      scrollTo: { y: target, autoKill: false, offsetY: 200 },
      ease: Power4.easeOut
    });
  },
  false
);
