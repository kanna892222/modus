// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: {
    pageData,
    index: {
      list: 0
    }
  },
  computed: {
    nowList() {
      return this.index.list !== null
        ? this.pageData.table[this.index.list].list
        : null;
    }
  },
  mounted() {}
});

let swiper = new Swiper(".proin_swiper-container", {
  navigation: {
    nextEl: ".proin-next",
    prevEl: ".proin-prev"
  },
  //循環
  loop: true,
  speed: 1000
  //自動播放
});

let proInAni = new TimelineLite();
proInAni
  .from(".proin_swiper-container", 0.5, {
    delay: 1,
    x: -30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".proin__title", 0.5, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".slide__btns", 0.5, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".font__img", 0.5, {
    x: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".proin__apptxt", 0.5, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  });
