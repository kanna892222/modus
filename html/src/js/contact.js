// vue

const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {}
});

$(".list__dsk>a").removeClass("active");
$("#contact").addClass("active");
//nav bar

$("#register_form")
  .validator()

  .on("submit", function(e) {
    if (e.isDefaultPrevented()) {
      // 未驗證通過 則不處理
      // console.log("沒有送出");
      return;
    } else {
      // 通過送出表單
      //console.log($("#register_form").serialize());
      $.ajax({
        type: "POST",
        url: "http://localhost:3000/test",
        data: $("#register_form").serialize(),
        dataType: "json",
        success: function success(response) {
          alert("已收到你聯絡資料");
        }
      }).done(function(data) {
        $("#register_form")
          .find(":text,textarea")
          .each(function() {
            $(this).val("");
          });
        $("#inputEmail").val("");
      });
    }
    e.preventDefault(); // 防止原始 form 提交表單
  });

let contactAni = new TimelineLite();
contactAni
  .from(".contact__img", 0.5, {
    delay: 2,
    x: -30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".font__contact", 0.5, {
    x: -30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".info__totalarea", 0.5, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  })
  .from(".info__from", 0.5, {
    y: 30,
    opacity: 0,
    clearProps: "opacity, transform"
  });
//info__totalarea
//info__from
