// vue
const vm = new Vue({
  el: "#mainContainer",
  mixins: [vueMixins],
  data: { pageData },
  mounted() {}
});

if (window.matchMedia(bkpDsk).matches) {
  window.addEventListener("load", function() {
    let galleryTop = new Swiper(".gallery-top", {
      pagination: {
        //綁定頁數顯式
        el: ".swiper-pagination",
        type: "fraction"
      },
      navigation: {
        nextEl: ".pro-next",
        prevEl: ".pro-prev"
      },
      loop: true,
      loopedSlides: 4,
      effect: "fade",
      fadeEffect: { crossFade: true },
      speed: 1000,
      autoplay: {
        delay: 8000,
        disableOnInteraction: false
      },
      on: {
        slideChangeTransitionStart: function() {
          let title1 = pageData.slide[this.realIndex].title;
          let title2 = pageData.slide[this.realIndex].class;
          let splitTop = title1.split("");
          let splitBottom = title2.split("");
          $(".en__title").empty();
          $(".class").empty();
          splitTop.forEach(ele => {
            $(".en__title").append($("<span class='type__x'>").text(ele));
          });
          splitBottom.forEach(ele => {
            $(".class").append($("<span class='type__x'>").text(ele));
          });
          $(".en__title>.type__x").each(function(index, element) {
            setTimeout(() => {
              $(this).addClass("flip");
            }, 40 * index);
          });
          $(".class>.type__x").each(function(index, element) {
            setTimeout(() => {
              $(this).addClass("flip");
            }, 80 * index);
          });
        }
      }
    });
    let galleryThumbs = new Swiper(".gallery-thumbs", {
      touchRatio: 0.2,
      loop: true,
      loopedSlides: 4,
      slidesPerView: "3",
      spaceBetween: 20,
      speed: 1000
    });

    galleryTop.controller.control = galleryThumbs;
    galleryThumbs.controller.control = galleryTop;

    $(".list__dsk>a").removeClass("active");
    $("#product-slide").addClass("active");
    galleryTop.el.onmouseover = function() {
      galleryTop.autoplay.stop();
    };
    galleryTop.el.onmouseout = function() {
      galleryTop.autoplay.start();
    };
  });
}

//======================================斷點用swiper

if (window.matchMedia(bkpMbl).matches) {
  window.addEventListener("load", function() {
    let dataLength = pageData.slide.length;
    // console.log(dataLength);
    let setData = dataLength * 450 + 60;
    // console.log(setData);
    $(".slide__wrap").css("height", setData);
  });
}
