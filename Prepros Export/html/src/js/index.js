// vue
const vm = new Vue({
  el: '#mainContainer',
  mixins: [vueMixins],
  data: { pageData },
});

// swiper
const mySwiper = new Swiper('.swiper-container', {
  preloadImages: false,
  lazy: {
    loadPrevNext: true,
  },
});
