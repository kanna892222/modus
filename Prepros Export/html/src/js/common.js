// common
const bkpDsk = '(min-width: 1024px)';
const bkpMbl = '(max-width: 1023.98px)';
const mdaHvr = '(hover: hover)';

const $id = id => document.getElementById(id);
const $qs = el => document.querySelector(el);
const $qsa = el => document.querySelectorAll(el);

const vueMixins = {
  delimiters: ['<%', '%>'],
  filters: {
    zerofill(value) {
      return value.toString().length === 1 ? `0${value}` : value;
    },
    zerofillPlus1(value) {
      value++;
      return value.toString().length === 1 ? `0${value}` : value;
    }
  },
  data: { commonData },
  computed: {
    // location.pathname
    nowPathName() {
      return location.pathname.split('/').pop();
    },
    // windowSize
    windowSize() {
      return window.matchMedia(bkpDsk).matches ? 'dsk' : 'mbl';
    }
  },
  created() {
    // 判斷作業系統
    if (navigator.userAgent.includes('Mac OS X')) {
      document.body.classList.add('mac');
    }

    // windowSize
    window.addEventListener(
      'resize',
      _.debounce(() => {
        let wsz = window.matchMedia(bkpDsk).matches ? 'dsk' : 'mbl';
        if (this.windowSize !== wsz) {
          location.reload();
        }
      }, 150)
    );
  },
  mounted() {
    // lazySizesConfig
    lazySizes.init();
  }
};

document.addEventListener('DOMContentLoaded', () => {});
